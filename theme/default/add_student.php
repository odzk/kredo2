<?php require('../../include/functions.php');

$header = new header('Kredo IT Abroad Manage Student');
$header->start_header();

$get_schedule = new connect_db(1, 'student_schedule', '', '');
$get_schedule->get_schedule();

$student_id = $get_schedule->student_id;
$student_id = explode(',', $student_id);

$week = $get_schedule->week;
$week_count = explode(',', $week);


$time = $get_schedule->schedule_time;
$time_schedule = explode(',', $time);

$program = $get_schedule->course_program;
$course_program= explode(',', $program);

$teacher = $get_schedule->teacher;
$teacher_name = explode(',', $teacher);

$room = $get_schedule->room;
$class_room = explode(',', $room);


?>

<body>
<!-- header -->
<?php $header->navigation(); ?>

<!-- /Header -->

<!-- Main -->

<!-- Side Navigation -->
<?php $header->side_navigation(); ?>
<!-- End of Side Navigation -->
        <form class="form-vertical">
        <div class="col-sm-9" style="margin-top: 40px;">
        <h2 class="text-center">Manage Student</h2>
        <hr>
        <div class="col-sm-6">
        <h2 class="text-center">Student's Information</h2>
        <label for="student_name" style="margin-top:10px;">Student Name: 名前 </label>
        <input type="text" name="student_name" id="student_name" class="form-control" placeholder="Enter Complete Name">
 
        <label for="student_nick" style="margin-top:10px;">Student's Nick Name:  ニックネーム（あれば）</label>
        <input type="text" name="student_nick" id="student_nick" class="form-control" placeholder="Enter Student Nick Name">
 
        <label for="batch_number" style="margin-top:10px;">Batch Number: グループ番号 </label>
        <input type="text" name="batch_number" id="batch_number" class="form-control" placeholder="Batch Number">
    
        <label for="student_id" style="margin-top:10px;">Student ID: 学生証 </label>
        <input type="text" name="student_id" id="student_id" class="form-control" placeholder="Student ID">

        <label for="gender" style="margin-top:10px;">Gender: 性別 </label>
        <select name="gender" class="form-control">
        <option value="male">Male 男性</option>
        <option value="female">Female 女性</option>
        </select>

        <label for="birth_place" style="margin-top:10px;">Birth Place: 出身地 </label>
        <input type="text" name="birth_place" id="birth_place" class="form-control" placeholder="Birth Place">

        <label for="birth_date" style="margin-top:10px;">Date of Birth: 誕生日 </label>
        <input type="date" name="birth_date" id="birth_date" class="form-control" placeholder="mm/dd/yyyy">

        <label for="age" style="margin-top:10px;">Age 年齢</label>
        <input type="number" name="age" id="age" class="form-control">
        <?php $access="admin" ?>
        <?php if ($access == "admin") {
        echo '<div style="display: block;">'; } else {
        echo '<div style="display: none;">'; 
        } ?>
        <label style="margin-top:30px;">To be filled out by Kredo Staff:</label><br>

        <label style="margin-top:10px;">IT Level</label><br>
        <input type="radio" name="it_level" value="beginner"> Beginner<br>
        <input type="radio" name="it_level" value="average"> Average<br>
        <input type="radio" name="it_level" value="advance"> Advance<br><br>

        <label style="margin-top:10px;">ESL Level</label><br>
        <input type="radio" name="esl_level" value="beginner"> Beginner<br>
        <input type="radio" name="esl_level" value="average"> Average<br>
        <input type="radio" name="esl_level" value="advance"> Advance<br><br>
    

        <label style="margin-top:10px;">Course Category</label><br>
        <input type="checkbox" name="course_category" value="wdes1"> Web Design I<br>
        <input type="checkbox" name="course_category" value="wdes2"> Web Design II<br>
        <input type="checkbox" name="course_category" value="wdev1"> Web Development I<br>
        <input type="checkbox" name="course_category" value="wdev2"> Web Development II<br>

        <label for="duration" style="margin-top:30px;">Duration: 期間</label>
        <input type="date" name="duration" class="form-control" id="duration" placeholder="Number of Months">
        <label for="date_started" style="margin-top:10px;">Date Started: 開始日</label>
        <input type="date" name="date_started" id="date_started" class="form-control" placeholder="mm/dd/yyyy">
        <label for="date_completed" style="margin-top:10px;">Date Completed: 終了日</label>
        <input type="date" name="date_completed" id="date_completed" class="form-control" placeholder="mm/dd/yyyy">
        <label for="student_other" style="margin-top: 10px;">Other Information About the Student: その他</label>
        <textarea class="form-control" name="student_other" placeholder="Other info like VIP, high level IT skill etc." id="student_other" rows="3"></textarea>
    

        <label style="margin-top: 30px">To be filled out after graduation:</label>
        <label for="student_portfolio">Student Portfolio Website Link</label>
        <input type="text" class="form-control" id="student_portfolio" name="student_portfolio" placeholder="http://www.example.com">
        </div>
        </div>
        <div class="col-sm-6" style="border-left: 1px solid #eee;">
        <h2 class="text-center">Assessment</h2>
        <label for="hobbies">What are your hobbies? 趣味 </label>
        <textarea class="form-control" name="hobbies" rows="3"></textarea>

        <label for="it_skill" style="margin-top: 20px;">What are the IT skills you currently have? ITスキル</label>
        <textarea class="form-control" name="it_skill" rows="3"></textarea>

        <label for="english_skill" style="margin-top: 20px;">How is your English Skills? 英語スキル</label>
        <textarea class="form-control" name="english_skill" rows="3"></textarea>

        <label for="purpose" style="margin-top: 20px;">What is your purpose in coming here to Kredo? どういう目的で来ている</label>
        <textarea class="form-control" name="purpose" rows="3"></textarea>

        <label for="website_type" style="margin-top: 20px;">What kind of website are you planning to make? どういうwebサイトを作りたいか</label>
        <textarea class="form-control" name="website_type" rows="3"></textarea>

        <label for="learn_it" style="margin-top: 20px;">What IT skills do you want to acquire from this school? ITのレッスンでどういったスキルを身に付けたいか</label>
        <textarea class="form-control" name="learn_it" rows="3"></textarea>

        <label for="goal" style="margin-top: 20px;">What do you want to do in the future? What is your goal? 将来のやりたい仕事は？夢は？</label>
        <textarea class="form-control" name="goal" rows="3"></textarea>

        <label for="want_kredo" style="margin-top: 20px;">What are you looking for at kredo? kredoに求めるもの</label>
        <textarea class="form-control" name="want_kredo" rows="3"></textarea>

        <label for="dream_job" style="margin-top: 20px;">What do you want to do in the future? What is your dream job? 将来のやりたい仕事は？夢は？</label>
        <textarea class="form-control" name="dream_job" rows="3"></textarea>

        <label for="desired_course" style="margin-top: 20px;">Desired Course? 希望コース </label>
        <textarea class="form-control" name="desired_course" rows="3"></textarea>

        <label for="it_company" style="margin-top: 20px;">What IT company do you want to work with? IT企業に就職したいか？ </label>
        <textarea class="form-control" name="it_company" rows="3"></textarea>
        
        <button class="btn btn-success form-control" style="margin-top: 20px" onclick="saveRecord()">Save</button>
        </form>
        </div>


</div>

        </div>
    </div>
</div>
<footer class="text-center">Student Information System by Odysseus Ambut. Version 0.9beta <a href="https://bitbucket.org/odzk/kredo2">Click Here to View Source Code from Bitbucket</a></footer>

<script>
function changeDate() {
   var week = document.getElementById('changeWeek').value;
   var day = document.getElementById('changeDay').value;
   window.location = 'assign.php?week=' + week + '&day=' + day;
}
</script>

    </body>
</html>