<?php require('../../include/functions.php');

$header = new header('Kredo IT Abroad Student Feedback System');
$header->start_header();

$get_schedule = new connect_db(1, 'student_schedule', '', '');
$get_schedule->get_schedule();

$student_id = $get_schedule->student_id;
$student_id = explode(',', $student_id);

$week = $get_schedule->week;
$week_count = explode(',', $week);


$time = $get_schedule->schedule_time;
$time_schedule = explode(',', $time);

$program = $get_schedule->course_program;
$course_program= explode(',', $program);

$teacher = $get_schedule->teacher;
$teacher_name = explode(',', $teacher);

$room = $get_schedule->room;
$class_room = explode(',', $room);


?>

<body>
<!-- header -->
<?php $header->navigation(); ?>

<!-- /Header -->

<!-- Main -->

<!-- Side Navigation -->
<?php $header->side_navigation(); ?>
<!-- End of Side Navigation -->
        <form class="form-vertical">
        <div class="col-sm-9" style="margin-top: 40px;">
        <h2 class="text-center">Student / Teacher Feedback</h2>
        <hr>
        <div class="col-sm-6">
        <label for="student_name" style="margin-top: 10px;">Student Name: </label>
        <select id="student_name" name="student_name" class="form-control">
        <option name="studentName" value="" class="form-control">Shunpei Use</option>
        </select>
        <label for="date" style="margin-top: 10px;">Date:</label>
        <input type="date" name="date" class="form-control" id="date" placeholder="mm/dd/yyyy">
        <label for="course_program" style="margin-top: 10px;">Course Program: </label>
        <input type="text" name="course_program" id="course_program" class="form-control">
        <label for="course_title" style="margin-top: 10px;">Course Title: </label>
        <input type="text" name="student_rate" id="course_title" class="form-control">
        <label for="student_rate" style="margin-top: 10px;">Today's Student Rate: </label>
        <input type="number" name="student_rate" id="student_rate" class="form-control" placeholder="1 to 10" required>

        </form>
        </div>
        <div class="col-sm-6">
        <label for="notes">What did you discuss today?</label>
        <textarea class="form-control" name="notes1" placeholder="What did you discuss today?" rows="3"></textarea>

        <label for="notes2" style="margin-top: 20px;">Which part of the topic students have problems with?</label>
        <textarea class="form-control" name="notes2" placeholder="Which part of the topic students have problems with?" rows="3"></textarea>

        <label for="notes3" style="margin-top: 20px;">What is/are the assignment for students?</label>
        <textarea class="form-control" name="notes3" placeholder="What is/are the assignment for students?" rows="3"></textarea>

        <label for="notes4" style="margin-top: 20px;">What is the next topic?</label>
        <textarea class="form-control" name="notes4" placeholder="What is the next topic?" rows="3"></textarea>


        <button class="btn btn-success form-control" style="margin-top: 20px" onclick="saveRecord()">Save</button>
        </div>


</div>
<hr>
  <div class="panel-body col-sm-12">
  <h3 class="text-center">History</h3>
  <hr>
  <h4 class="text-center">IT Class</h4>
  <div class="col-sm-4" style="margin-bottom: 20px;">
  <label for="student_name">Filter Student Name: </label>
    <select id="student_name" name="student_name" class="form-control">
    <option name="studentName" value="" class="form-control">Shunpei Use</option>
    </select>
    <button class="btn btn-default" style="margin-top: 10px;">Change</button>
    </div>

    <div class="col-sm-4" style="margin-bottom: 20px;">
    <label for="filter_date">Filter Date: </label><br>
    <input type="date" name="filter_date" id="filter_date" placeholder="mm/dd/yyyy" class="form-control">
    <button class="btn btn-default" style="margin-top: 10px;">Change</button>
    </div>
  <table class="table table-bordered table-hover">
    <tr>
        <th>Date</th>
        <th>Teacher</th>
        <th>Course Program</th>
        <th>Course Title</th>
        <th>Dicuss Today</th>
        <th>Student's Difficulties</th>
        <th>Student's Assignment</th>
        <th>Next Topic</th>
    </tr>
    <tr>
        <td>06/01/2017</td>
        <td>Ody</td>
        <td>Adobe Illustrator</td>
        <td>Introduction</td>
        <td>We discussed about Adobe Illustrator</td>
        <td>Can't understand some portion of the topic</td>
        <td>Study in advance about Basic Menu</td>
        <td>Basic Menu</td>
    </tr>

    </table>


  <h4 class="text-center" style="margin-top: 50px;">ESL Class</h4>
  <table class="table table-bordered table-hover">
    <tr>
        <th>Date</th>
        <th>Teacher</th>
        <th>Course Program</th>
        <th>Course Title</th>
        <th>Dicuss Today</th>
        <th>Student's Difficulties</th>
        <th>Student's Assignment</th>
        <th>Next Topic</th>
    </tr>
    <tr>
        <td>06/01/2017</td>
        <td>Ody</td>
        <td>Adobe Illustrator</td>
        <td>Introduction</td>
        <td>We discussed about Adobe Illustrator</td>
        <td>Can't understand some portion of the topic</td>
        <td>Study in advance about Basic Menu</td>
        <td>Basic Menu</td>
    </tr>

    </table>
        </div>
    </div>
</div>
<footer class="text-center">Student Information System by Odysseus Ambut. Version 0.9beta <a href="https://bitbucket.org/odzk/kredo2">Click Here to View Source Code from Bitbucket</a></footer>

<script>
function changeDate() {
   var week = document.getElementById('changeWeek').value;
   var day = document.getElementById('changeDay').value;
   window.location = 'assign.php?week=' + week + '&day=' + day;
}
</script>

    </body>
</html>