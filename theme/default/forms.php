<?php require('../../include/functions.php'); 
$header = new header('Kredo IT Abroad Student Admin');
$header->start_header();
?>
<body>
<!-- header -->

<?php $header->navigation(); ?>

<!-- /Header -->

<!-- Main -->
<?php $header->side_navigation(); ?>
    
        <div class="col-sm-9" style="margin-top: 80px;">

            <!-- column 2 -->
            <ul class="list-inline pull-right">
                <li><a href="#"><i class="glyphicon glyphicon-cog"></i></a></li>
                <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-comment"></i><span class="count">3</span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">1. Is there a way..</a></li>
                        <li><a href="#">2. Hello, admin. I would..</a></li>
                        <li><a href="#"><strong>All messages</strong></a></li>
                    </ul>
                </li> -->
                <li><a href="#"><i class="glyphicon glyphicon-user"></i></a></li>
               <!-- <li><a title="Add Widget" data-toggle="modal" href="#addWidgetModal"><span class="glyphicon glyphicon-plus-sign"></span> Add Widget</a></li> -->
            </ul>
            <a href="#"><strong><i class="glyphicon glyphicon-dashboard"></i> My Dashboard</strong></a>
            <hr>

            <div class="row">
                <!-- center left-->
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Teachers Resources</h4></div>
                        <div class="panel-body">
                        <h4>Online Forms</h4>
                        <table class="table table-bordered table-hover">
                        <tr>
                            <th class="text-center col-sm-6">Type of Form</th>
                            <th class="text-center col-sm-6">Description</th>
                        </tr>
                        <tr>
                            <td class="text-center">
                        <a href="https://docs.google.com/document/d/1_yO9TFNsOdMTh58OZd8nXRk6Ek6j8AOw8LWcLo8ryA8/edit" target="_blank">Incident Report</a>
                            </td>
                            <td class="text-center">Incident Report Form</td>
                        </tr>
                        <tr>
                            <td class="text-center">
                        <a href="https://docs.google.com/document/d/1gCb2_tLEs72KC0ELkkLH4CRA0jVLy9qXPQjg7fUTMnY/edit" target="_blank">Teacher's Evaluation Form</a></td>
                        <td class="text-center">Use for Evaluating Teachers on a time to time basis, this form will be the basis for teacher's increase in salary and/or regularization</td>
                        </tr>
                        <tr>
                            <td class="text-center">
                        <a href="https://docs.google.com/document/d/13MagHTkCmYGlIJvdrcq2gf3gwRKJsW9iLEPfs9b6LTA/edit" target="_blank">Certificate of Employment</a></td>
                            <td class="text-center">Certificate of Employment</td>
                        </tr>
                        <tr>
                            <td class="text-center">
                        <a href="https://drive.google.com/file/d/0BwMHQVBOAixQY09vVWUxM0NfbWc/view?usp=sharing" target="_blank">Certificate of Completion</a></td>
                            <td class="text-center">Certificate of Completion for Graduating Students</td>
                        </tr>
                        <tr>
                            <td class="text-center">
                        <a href="https://docs.google.com/document/d/1MhkRGM5a1XPweaVuVqof3cacE0UCS53X5VFIFjoKTV8/edit" target="_blank">Meeting Report Form</a></td>
                            <td class="text-center">This form should be used everytime a meeting is conducted. </td>
                        </tr>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- /Main -->

<footer class="text-center">Student Information System by Odysseus Ambut. Version 0.9beta <a href="https://bitbucket.org/odzk/kredo2">Click Here to View Source Code from Bitbucket</a></footer>
	</body>
</html>