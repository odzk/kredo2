<?php require('../../include/functions.php');

$header = new header('Kredo IT Abroad Student Schedule');
$header->start_header();

$get_schedule = new connect_db(1, 'student_schedule', '', '');
$get_schedule->get_schedule();

$student_id = $get_schedule->student_id;
$student_id = explode(',', $student_id);

$week = $get_schedule->week;
$week_count = explode(',', $week);


$time = $get_schedule->schedule_time;
$time_schedule = explode(',', $time);

$program = $get_schedule->course_program;
$course_program= explode(',', $program);

$teacher = $get_schedule->teacher;
$teacher_name = explode(',', $teacher);

$room = $get_schedule->room;
$class_room = explode(',', $room);

$get_urlw = $_GET['week'];
$get_urld = $_GET['day'];

?>

<body>
<!-- header -->
<?php $header->navigation(); ?>
<!-- /Header -->

<!-- Main -->

<!-- Side Navigation -->
<?php $header->side_navigation(); ?>
<!-- End of Side Navigation -->
        <div class="col-sm-9" style="margin-top: 40px;">
        <h2 class="text-center">Assign Schedule</h2>
        <h4>Student Name: <select id="studentName" name="studentName">
        <option name="studentName" value="">Shunpei Use</option>
        </select> <button class="btn btn-small btn-default" onclick="studentName()">Apply</button></h4>
        <h4><select id="changeWeek">
        <?php foreach($week_count as $week_value) { ?>
        <option name="changeWeek" value="<?php echo $week_value; ?>" <?php if($get_urlw == $week_value) { echo 'selected'; } ?>><?php echo 'Week ' . $week_value; ?></option>
            <?php } ?>
        </select>
        <select id="changeDay">
        <?php for ($x=1; $x<=5; $x++) { ?>
            <option name="changeDay" value="<?php echo $x; ?>" <?php if($get_urld == $x) { echo 'selected'; } ?>><?php echo 'Day ' . $x; ?></option>
            <?php } ?>
            </select> <button class="btn btn-small btn-default" onclick="changeDate()">Submit</button>
        </h4> 
        <h4>Load Defaults : 
        <select id="loadDefaults">
        <?php foreach($week_count as $week_value) { ?>
        <option name="loadWeek" value="<?php echo $week_value; ?>"><?php echo 'Week ' . $week_value; ?> </option>
            <?php } ?>
        </select> <button class="btn btn-small btn-default" onclick="loadDefaults()">Apply</button>
        </h4>

        <button class="btn btn-success center-block" onclick="saveRecord()">Save</button>


<table class="table table-bordered table-hover">
<tr>
   <th class="text-center">Time</th>
   <th class="text-center">Course Program</th>
   <th class="text-center">Teacher</th>
   <th class="text-center">Room</th>
</tr>

<?php 
// if 1

$z = 14; //count($time_schedule);

if ($get_urlw == 1) {
    if ($get_urld == 1) {
    $y = 0; } }

if ($get_urlw == 1) {
    if ($get_urld >= 2) {
    $y = ($z * ($get_urld-1) + $get_urld)-1; } }

// $xy = 1;
// for ($x=2; $x<=4; $x++) {
//     if($get_urld == $x) {
//     $y = (($z * (5 * ($xy))) + 5 * ($xy)) * $get_urld;
//     }   elseif ($get_urld >= 2) {
//     $y = ((($z * (5 * ($xy))) + 5 * ($xy))) + ($z * ($get_urld - 1)) + ($get_urld - 1);
//     } $xy++; }

if ($get_urlw == 2) {
    if ($get_urld == 1) {
    $y = (($z * 5) + 5) * $get_urld;
}   elseif ($get_urld >= 2) {
    $y = ((($z * 5) + 5)) + ($z * ($get_urld - 1)) + ($get_urld - 1);
} }

if ($get_urlw == 3) {
    if ($get_urld == 1) {
    $y = (($z * 10) + 10) * $get_urld;
}   elseif ($get_urld >= 2) {
    $y = ((($z * 10) + 10)) + ($z * ($get_urld - 1)) + ($get_urld - 1);
} }

if ($get_urlw == 4) {
    if ($get_urld == 1) {
    $y = (($z * 15) + 15) * $get_urld;
}   elseif ($get_urld >= 2) {
    $y = ((($z * 15) + 15)) + ($z * ($get_urld - 1)) + ($get_urld - 1);
} }

echo $y;
for($x=0; $x<=$z; $x++) { ?>
<tr>
   <td class="text-center"><?php echo $time_schedule[$y]; ?></td>
   <td class="text-center"><select id="course_program" name="course_program">
   <option><?php echo $course_program[$y]; ?></option>
   </select>
   </td>


   <td class="text-center"><?php echo $teacher_name[$y]; ?></td>
   <td class="text-center"><?php echo $class_room[$y]; ?></td>
</tr>
<?php  $y++;  } ?>
</table>

<hr>
  <div class="panel-body col-sm-6">
  <h3 class="text-center">Request to Change Schedule</h3>

                            <form class="form form-vertical">
                                <div class="control-group">
                                    <label>Name</label>
                                    <div class="controls">
                                        <input type="text" class="form-control" placeholder="Kredo Admin Scheduler" readonly>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label>Date</label>
                                    <div class="controls">
                                        <input type="date" class="form-control">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label>Reason</label>
                                    <div class="controls">
                                        <textarea class="form-control" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label></label>
                                    <div class="controls">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                <div class="col-sm-6" style="margin-top: 90px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Announcements:</h4></div>
                        <div class="panel-body">
                            <p>There is no announcement at this time!</p>
                        </div>
                    </div>
</div>



                </div>
    </div>
<footer class="text-center">Student Information System by Odysseus Ambut. Version 0.9beta <a href="https://bitbucket.org/odzk/kredo2">Click Here to View Source Code from Bitbucket</a></footer>

<script>
function changeDate() {
   var week = document.getElementById('changeWeek').value;
   var day = document.getElementById('changeDay').value;
   window.location = 'assign.php?week=' + week + '&day=' + day;
}
</script>

	</body>
</html>