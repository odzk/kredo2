-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 17, 2017 at 03:47 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kredo`
--

-- --------------------------------------------------------

--
-- Table structure for table `course_program`
--

CREATE TABLE `course_program` (
  `id` int(11) NOT NULL,
  `course_title` varchar(100) NOT NULL,
  `course_topic` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_program`
--

INSERT INTO `course_program` (`id`, `course_title`, `course_topic`) VALUES
(1, 'Adobe Illustrator', 'Chapter 1: Getting Started, Part 1: What is Adobe Illustrator?, Part 2: Why use Adobe Illustrator?, Part 3: What is the difference between Adobe Photoshop and Adobe Illustrator?, Chapter 2: Adobe Illustrator UI (User Interface), Part 1: Tools Panel, Part 2: Options Bar, Part 3: Menu, Part 4: Tabbed Window Document, Part 5: Illustrator Panels, Part 6: Workspace Options, Chapter 3A: Basic Illustrator Features Part 1, Part 1: Basic Shapes, Rectangle Tool, Rounded Rectangle Tool, Ellipse Tool, Polygon Tool, Star Tool, ACTIVITY 1 - MAKING FLAGS, Chapter 3B: Basic Illustrator Feature Part 2, Part 1: Pen tool, ACTIVITY 2 - MAKE A CARTOON FROM PICTURE, Part 2: Transforming, Part 3: Layers & Grouping, Part 4: Pathfinder, Part 5. Type Tool / Text, Part 5: Fill/Stroke/Gradients and Color, Part 6: Adding Gradients to Text, ACTIVITY 3: MAKE A BUSINESS CARD, Chapter 4: Effects and Appearances, Part 1: Warp Effects, Part 2: Stylize, Part 3: 3D Effects, ACTIVITY 4: MAKE A FLYER, ACTIVITY 5: MAKE A LOGO');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `teacher_name` text NOT NULL,
  `student_name` text NOT NULL,
  `date` text NOT NULL,
  `course_program` text NOT NULL,
  `course_title` text NOT NULL,
  `discuss` varchar(5000) NOT NULL,
  `problem` varchar(5000) NOT NULL,
  `assignment` varchar(5000) NOT NULL,
  `next_topic` varchar(5000) NOT NULL,
  `progress` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_access`
--

CREATE TABLE `student_access` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_course`
--

CREATE TABLE `student_course` (
  `id` int(11) NOT NULL,
  `course_name` varchar(5000) NOT NULL,
  `course_duration` text NOT NULL,
  `grade` text NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_course`
--

INSERT INTO `student_course` (`id`, `course_name`, `course_duration`, `grade`, `remarks`) VALUES
(1, 'Adobe Photoshop,Adobe Illustrator,HTML/CSS,Wordpress,Bootstrap,Javascript,HTML/CSS II (Portfolio)', '8,12,40,20,20,20,40', '90,90,90,92,90,90,n/a', 'Competent/Completed,Competent/Completed,Competent/Completed,Competent/Completed,Competent/Completed,Competent/Completed,n/a');

-- --------------------------------------------------------

--
-- Table structure for table `student_profile`
--

CREATE TABLE `student_profile` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `batch` text NOT NULL,
  `student_id` text NOT NULL,
  `student_name` text NOT NULL,
  `gender` text NOT NULL,
  `birth_place` text NOT NULL,
  `birth_date` text NOT NULL,
  `age` int(11) NOT NULL,
  `course` text NOT NULL,
  `duration` int(11) NOT NULL,
  `it_level` text NOT NULL,
  `esl_level` text NOT NULL,
  `category` text NOT NULL,
  `date_started` text NOT NULL,
  `date_completed` text NOT NULL,
  `other` varchar(1000) NOT NULL,
  `hobbies` varchar(500) NOT NULL,
  `it_skill` varchar(500) NOT NULL,
  `english_skill` varchar(500) NOT NULL,
  `purpose` varchar(500) NOT NULL,
  `website_type` varchar(500) NOT NULL,
  `learn_it` varchar(500) NOT NULL,
  `goal` varchar(500) NOT NULL,
  `want_kredo` varchar(500) NOT NULL,
  `dream_job` varchar(500) NOT NULL,
  `desired_course` varchar(500) NOT NULL,
  `it_company` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_profile`
--

INSERT INTO `student_profile` (`id`, `timestamp`, `batch`, `student_id`, `student_name`, `gender`, `birth_place`, `birth_date`, `age`, `course`, `duration`, `it_level`, `esl_level`, `category`, `date_started`, `date_completed`, `other`, `hobbies`, `it_skill`, `english_skill`, `purpose`, `website_type`, `learn_it`, `goal`, `want_kredo`, `dream_job`, `desired_course`, `it_company`) VALUES
(1, '2017-04-12 14:27:32', '0306', '0', 'Hiromi Harisawa', '', '', '', 0, 'IT Web Design', 2, '', '', 'Web Design I and Web Design II', '03/06/2017', '04/28/2017', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_progress`
--

CREATE TABLE `student_progress` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_status` tinyint(4) NOT NULL,
  `course_feedback` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_progress`
--

INSERT INTO `student_progress` (`id`, `student_id`, `course_status`, `course_feedback`) VALUES
(1, 0, 0, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `student_schedule`
--

CREATE TABLE `student_schedule` (
  `id` int(11) NOT NULL,
  `student_id` text NOT NULL,
  `week` text NOT NULL,
  `schedule_time` text NOT NULL,
  `course_program` text NOT NULL,
  `teacher` text NOT NULL,
  `room` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_schedule`
--

INSERT INTO `student_schedule` (`id`, `student_id`, `week`, `schedule_time`, `course_program`, `teacher`, `room`) VALUES
(1, '050101', '1,2,3,4', '7:00-8:00,8:00-9:00,9:00-10:00,10:00-11:00,11:00-12:00,12:00-13:00,13:00-14:00,14:00-15:00,15:00-16:00,16:00-17:00,17:00-18:00,18:00-19:00,19:00-20:00,20:00-21:00,21:00-22:00', 'Adobe Photoshop, Adobe Photoshop, Adobe Photoshop, Adobe Illustrator', 'Ody,Ody,Ody,Ody', 'A,A,A,A');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `id` int(11) NOT NULL,
  `student_id` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`id`, `student_id`, `email`, `password`) VALUES
(1, '', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course_program`
--
ALTER TABLE `course_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_access`
--
ALTER TABLE `student_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_course`
--
ALTER TABLE `student_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_profile`
--
ALTER TABLE `student_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_progress`
--
ALTER TABLE `student_progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_schedule`
--
ALTER TABLE `student_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course_program`
--
ALTER TABLE `course_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_access`
--
ALTER TABLE `student_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_course`
--
ALTER TABLE `student_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_profile`
--
ALTER TABLE `student_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_progress`
--
ALTER TABLE `student_progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_schedule`
--
ALTER TABLE `student_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
