<?php 
require_once('functions.php');

$get_info = new connect_db('1','student_profile', '', '');
$get_course = new connect_db('1', 'student_course', '','');
$get_info->student_profile();
$get_course->student_course();
$full_course = $get_course->course_name;
$full_duration = $get_course->course_duration;
$full_grade = $get_course->grade;
$full_remarks = $get_course->remarks;
$each_course = explode(",", $full_course);
$each_duration = explode(",", $full_duration);
$each_grade = explode(",", $full_grade);
$each_remarks = explode(",", $full_remarks);

?>

<!DOCTYPE html>
<html>
<?php 
$header = new header('Student Scorecard');
$header->start_header();
?>

<body>


<div class="container">
	<div class="col-sm-12">
	<img src="img/kredo-logo.jpg" width="150px">
	</div>
<div class="col-sm-2" style="margin-top: 50px;">

<?php
$menu = new menu('scorecard');
$menu->active_menu();
?>

</div>

<div class="col-sm-10">
<table class="table table-bordered table-hover" style="margin-top: 50px;">
<tr>
	<th class="text-center success" colspan="5"><?php echo $header->title; ?></th>
</tr>

<tr>
	<th>Student Name:</th>
	<td colspan="3"><?php echo $get_info->student_name; ?></td>
</tr>

<tr>
	<th>Batch Number</th>
	<td colspan="3"><?php echo $get_info->batch; ?></td>
</tr>

<tr>
	<th>Course:</th>
	<td colspan="3"><?php echo $get_info->course; ?></td>
</tr>

<tr>
	<th>Course Duration:</th>
	<td colspan="3"><?php echo $get_info->duration; ?> Months</td>
</tr>

<tr>
	<th>Program Category:</th>
	<td colspan="3"><?php echo $get_info->category; ?></td>
</tr>


<tr>
	<th>Date Started:</th>
	<td colspan="3"><?php echo $get_info->date_started; ?></td>
</tr>

<tr>
	<th>Date Completed:</th>
	<td colspan="3"><?php echo $get_info->date_completed; ?></td>
</tr>
</table>

<table class="table table-bordered table-hover">
<tr>
	<th class="text-center">Course Name</th>
	<th class="text-center">Course Duration (hrs)</th>
	<th class="text-center">Grade</th>
	<th class="text-center">Remarks</th>
</tr>

<?php foreach ($each_course as $key => $course)  {
	
	?>
<tr>
	<td class="text-center"><?php echo $course; ?></td>
	<td class="text-center"><?php echo $each_duration[$key]; ?></td>
	<td class="text-center"><?php echo $each_grade[$key]; ?></td>
	<td class="text-center"><?php echo $each_remarks[$key]; ?></td>
</tr>
<?php  } ?>

</table>
</div>
</div>

<?php footer(); ?>

</body>

<script>

</script>


</html>




