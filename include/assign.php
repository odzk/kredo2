<?php
require_once('functions.php');
$get_info = new connect_db('1','student_profile', '', '');
$get_info->student_profile();

?>

<!DOCTYPE html>
<html>
<?php
$header = new header('Student Schedule');
$header->start_header();
?>

<body>

<div class="container">
	<div class="col-sm-12">
	<img src="img/kredo-logo.jpg" width="150px">
	</div>

<div class="col-sm-2" style="margin-top: 50px;">

<?php
$menu = new menu('assign');
$menu->active_menu();
?>

</div>

<div class="col-sm-10">
<table class="table table-bordered table-hover" style="margin-top: 50px;">
<tr>
	<th class="text-center success" colspan="4">Student Schedule Assignment</th>
</tr>

<tr>
	<th>Student Name</th>
	<th colspan="3">Week</th>
</tr>

<tr>
	<td><select>
			<option>Hiromi Harisawa</option>
		</select>
	</td>
	<td colspan="3"><select>
		<?php for($x=1; $x<=12; $x++){ ?>
			<option><?php echo $x; ?></option>
		<?php } ?>
		</select>
	</td>
</tr>

<tr>
	<th>Time</th>
	<th>Course Program</th>
	<th>Teacher</th>
	<th>Room</th>
</tr>

<?php 
$x = 7;
$y = 8;
while ($x<=22) { ?>
<tr>
	<td><?php echo $x; ?>:00 - <?php echo $y; ?>:00</td>
	<td><select>
			<option>No Class</option>
			<option>Adobe Illustrator</option>
			<option>Adobe Photoshop</option>
		</select>
	</td>
	<td>
		<select>
			<option>Ody</option>
			<option>Zee</option>
			<option>Monique</option>
			<option>Diane</option>	
			<option>Mae</option>
			<option>Grace</option>
			<option>Chammy</option>	
			<option>Mikka</option>
			<option>Alex</option>	
		</select>
	</td>
	<td>
		<select>
			<option>A</option>
			<option>B</option>
			<option>C</option>
			<option>D</option>
		</select>
	</td>
</tr>
<?php $x++; $y++; } ?>

</table>
<div class="text-center">

<input type="submit" value="Apply" class="btn btn-success">
<input type="reset" value="Reset" class="btn btn-success">

</div>

</div>
</div>

<?php footer() ?>
