<?php

require_once('functions.php');

if(empty($_POST['submit'])){
  $email = $pword = $_POST['submit'] = '';
} else {

  $email = $_POST['email'];
  $pword = $_POST['pword'];

  $check_pass = new connect_db('', 'user_access', $email, $pword);
  $check_pass->checkPass();
}
?>

<!DOCTYPE html>
<html>
<?php 
$header = new header('Login Page');
$header->start_header();
?>


<body>
  <div class="conteiner">
  <div class="col-sm-offset-1 col-sm-4" style="margin-top: 60px;">
  <h2>Login Here</h2>

  <form method="POST" action="">
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="text" class="form-control" name="email" id="email">
  </div>
  <div class="form-group">
    <label for="pword">Password:</label>
    <input type="password" class="form-control" name="pword" id="pword">
  </div>
  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
  </form>

  </div>

  <div class="col-sm-7" style="margin-top:60px; border-left: 4px solid grey; height: 250px;">
    <img src="img/kredo-logo.jpg" class="img-responsive">
   <p style="color: grey; margin-top: 30px; margin-left: 30px;">Don't have an acount?  <a href="Register.php">Register here </a></p>
   </div>

  </div>
  </div>
</body>

</html>
