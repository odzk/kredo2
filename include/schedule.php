<?php
require_once('functions.php');
$get_info = new connect_db('1','student_profile', '', '');
$get_info->student_profile();


$type = $_GET['type'];

?>

<!DOCTYPE html>
<html>
<?php
$header = new header('Student Schedule');
$header->start_header();
?>

<body>

<div class="container">
	<div class="col-sm-12">
	<img src="img/kredo-logo.jpg" width="150px">
	</div>

<div class="col-sm-2" style="margin-top: 50px;">

<?php
$menu = new menu('schedule');
$menu->active_menu();
?>

</div>

<div class="col-sm-10">
<table class="table table-bordered table-hover" style="margin-top: 50px;">
<tr>
	<th class="text-center success" colspan="1"><span id="date"></span><span class="caret" id="changeDate" onclick="changeDate()"></span></th>
	<th class="text-center success" colspan="4">Student schedule-Daily</th>
</tr>

<tr>
	<th>Student Name:</th>
	<td colspan="3"><?php echo $get_info->student_name; ?></td></tr>

<tr>
	<th >Batch Number</th>
	<td colspan="3"><?php echo $get_info->batch; ?></td>
</tr>

<tr>
	<th>Course:</th>
	<td colspan="3"><?php echo $get_info->course; ?></td>
</tr>

<tr>
	<th>Course Duration:</th>
	<td colspan="3"><?php echo $get_info->duration; ?> Months</td>
</tr>

<tr>
	<th>Program Category:</th>
	<td colspan="3"><?php echo $get_info->category; ?></td>
</tr>


<tr>
	<th>Date Started:</th>
	<td colspan="3"><?php echo $get_info->date_started; ?></td>
</tr>

<tr>
	<th>Date Completed:</th>
	<td colspan="3"><?php echo $get_info->date_completed; ?></td>
</tr>

</table>

<table class="table table-bordered table-hover">

<?php if($type == 'daily'){

$get_info->student_schedule_daily();

} elseif($type == 'weekly'){

$get_info->student_schedule_weekly();

}
elseif($type == 'monthly'){

$get_info->student_schedule_monthly();

}
?>

<!--
<?php foreach ($each_course_topic as $key => $course_topic)  {

	?>
<tr>
	<td class="text-center col-sm-1">time</td>
	<td class="text-center col-sm-5"><?php echo $course_topic; ?></th>
	<td class="text-center col-sm-5"><?php if(!empty($each_course_status[$key])) { echo $each_course_status[$key]; } ?></th>
	<td class="text-center col-sm-1"><?php if(!empty($each_course_feedback[$key])) { echo $each_course_feedback[$key]; } ?></th>
</tr>

<?php } ?>
-->

</table>
</div>
</div>

<div class="text-center center-block">

<input type="submit" value="Edit All" class="btn btn-info">
<button class="btn btn-info">Back</button>

</div>
<script>
	var d = new Date();
	document.getElementById('date').innerHTML = d.toDateString();

	function changeDate() {
		document.getElementById('date').style.display = "none";
		input = document.createElement("input");
		input.type="date";
		input.id = "changeDate";
		document.getElementById("date").parentNode.insertBefore(input, document.getElementById("date"));

	}
</script>

<?php footer(); ?>
