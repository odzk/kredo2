<?php 

$program = $_GET['q'];

$id = 1;

if($program == 'ai') {
	$course_program = 'Course Program - Adobe Illustrator';
	$course_id = 1;
} elseif($program == 'ps') {
	$course_program = 'Course Program - Adobe Photoshop';
	$course_id = 2;
}

require_once('functions.php');
$get_info = new connect_db($id,'student_profile', '', '');
$get_info->student_profile();

$each_course_status = $each_course_feedback = "";

$program_course = new connect_db($course_id,'course_program', '', '');
$program_course->program_course();

$program_progress = new connect_db($course_id,'student_progress', '', '');
$program_progress->student_progress();

$all_course_topic = $program_course->course_topic;
$all_program_status = $program_progress->course_status;
$all_program_feedback = $program_progress->course_feedback;
$each_course_topic = explode(',', $all_course_topic);
$each_program_status = explode(',', $all_program_status);
$each_program_feedback = explode('<n>', $all_program_feedback);

if(empty($_POST['submit'])) {

$status = $feedback = $_POST['submit'] = '';

} else {

	$status = $_POST['status'];
	$feedback = $_POST['feedback'];
	$update_info = new connect_db($id,'student_progress', '', '');
	$update_info->student_progress_update($course_id, $status, $feedback);
}



?>

<!DOCTYPE html>
<html>
<?php 
$header = new header($course_program);
$header->start_header();
?>

<body>

<div class="container">
	<div class="col-sm-12">
	<img src="img/kredo-logo.jpg" width="150px">
	</div>

<div class="col-sm-2" style="margin-top: 50px;">

<?php
$menu = new menu('program');
$menu->active_menu();
?>

</div>

<div class="col-sm-10">
<table class="table table-bordered table-hover" style="margin-top: 50px;">
<tr>
	<th class="text-center success" colspan="5"><?php echo $header->title; ?></th>
</tr>

<tr>
	<th>Student Name:</th>
	<td colspan="3"><?php echo $get_info->student_name; ?></td>
</tr>

<tr>
	<th>Batch Number</th>
	<td colspan="3"><?php echo $get_info->batch; ?></td>
</tr>

<tr>
	<th>Course:</th>
	<td colspan="3"><?php echo $get_info->course; ?></td>
</tr>

<tr>
	<th>Course Duration:</th>
	<td colspan="3"><?php echo $get_info->duration; ?> Months</td>
</tr>

<tr>
	<th>Program Category:</th>
	<td colspan="3"><?php echo $get_info->category; ?></td>
</tr>


<tr>
	<th>Date Started:</th>
	<td colspan="3"><?php echo $get_info->date_started; ?></td>
</tr>

<tr>
	<th>Date Completed:</th>
	<td colspan="3"><?php echo $get_info->date_completed; ?></td>
</tr>

</table>

<table class="table table-bordered table-hover">

<tr>
	<th class="text-center">Course Topic</th>
	<th class="text-center">Status</th>
	<th class="text-center">Teacher's Notes</th>
</tr>
<form method="POST" action="">
<?php foreach ($each_course_topic as $key => $course_topic)  {
	if($each_program_status[$key] == '1') { $checked = 'checked'; } else { $checked = ''; } 
	?>
<tr>
	<td class="text-center col-sm-4"><span id="courseTopic<?php echo $key; ?>"><?php echo $course_topic; ?></span></td>
	<td class="text-center col-sm-1">
	<select name="status[]">
		<option value="N/A" <?php if($each_program_status[$key] == 'N/A') { echo 'selected'; } ?>>N/A</option>
		<option value="done" <?php if($each_program_status[$key] == 'done') { echo 'selected'; } ?>>Done</option>
		<option value="review" <?php if($each_program_status[$key] == 'review') { echo 'selected'; } ?>>Needs Review</option>
	</select></td>

	<td class="text-center col-sm-4"><textarea cols="30%" rows="5%" name="feedback[]" value="<?php echo $each_program_feedback[$key];?>"><?php echo $each_program_feedback[$key];?></textarea></td>

</tr>

<?php } ?>

</table>
</div>
</div>

<div class="text-center center-block">
<input type="submit" value="Save" name="submit" class="btn btn-info">
<button class="btn btn-info">Back</button>

</div>

</form>

<?php footer(); ?>

</body>
</html>
