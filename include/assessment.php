<?php
require_once('functions.php');

$id = 1;

$get_info = new connect_db($id,'student_profile');
$get_info->student_profile();
?>

<!DOCTYPE html>
<html>
<?php
$header = new header('Student Assessment');
$header->start_header();
?>

<body>


<div class="container">
	<div class="col-sm-12">
		<img src="img/kredo-logo.jpg" width="150px">
	</div>

	<div class="col-sm-2" style="margin-top: 50px;">
		<?php
		$menu = new menu('assessment');
		$menu->active_menu();
		?>
	</div>

	<div class="col-sm-10">
		<table class="table" style="margin-top: 50px;">
		<tr>
			<th class="text-center success" colspan="5"><?php echo $header->title; ?></th>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">1. What are your goals and objectives in learning IT?</p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">2. Do you usually work with a group or by yourself?</p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">3. Please define the following IT terms in your own words.</p>

					<form class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="html" style="font-weight:100; text-align:left;">- HTML</label>
							<div class="col-sm-6">
								<input class="form-control" id="html" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="css" style="font-weight:100; text-align:left;">- CSS</label>
							<div class="col-sm-6">
								<input class="form-control" id="css" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="ps" style="font-weight:100; text-align:left;">- Adobe Photoshop</label>
							<div class="col-sm-6">
								<input class="form-control" id="ps" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="ai" style="font-weight:100; text-align:left;">- Adobe illustrator</label>
							<div class="col-sm-6">
								<input class="form-control" id="ai" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="php" style="font-weight:100; text-align:left;">- PHP</label>
							<div class="col-sm-6">
								<input class="form-control" id="php" type="text" value="">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="sql" style="font-weight:100; text-align:left;">- MySQL</label>
							<div class="col-sm-6">
								<input class="form-control" id="sql" type="text" value="">
							</div>
						</div>
					</form>

				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">4. What is your Education Level? What is your Major</p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">5. Do you have any work experience? How long?</p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;">6. Did you go to any ESL or English School before? How long?</p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p><b>Assessor's Notes / Remarks:</b></p>
				<form>
				<label for="name"></label>
				<textarea id="name" placeholder="here..." cols="50" rows="5"></textarea>
				</form>
				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none;">
				<div class="well" style="margin-bottom: 0px;">
				<p style="margin-bottom: 15px;"><b>Level:</b></p>

					<form class="form-horizontal">
						<div class="form-group">
				      <label class="col-sm-4 control-label" for="q1" style="font-weight:100; text-align:left;">A. Beginner / Basic Level</label>
				      <div class="col-sm-6">
				        <input class="form-control" id="q1" type="text" value="">
				      </div>
				    </div>

						<div class="form-group">
				      <label class="col-sm-4 control-label" for="q2" style="font-weight:100; text-align:left;">B. Average / Intermediate Level</label>
				      <div class="col-sm-6">
				        <input class="form-control" id="q2" type="text" value="">
				      </div>
				    </div>

						<div class="form-group">
				      <label class="col-sm-4 control-label" for="q2" style="font-weight:100; text-align:left;">C. Advance Level</label>
				      <div class="col-sm-6">
				        <input class="form-control" id="q2" type="text" value="">
				      </div>
				    </div>
					</form>

				</div>
			</td>
		</tr>

		<tr>
			<td colspan="3" style="border: none; text-align: center; ">
			<button type="button" name="save"
			 style="border-radius: 30px;
			 background-color: #eee;
			 width: 150px;
			 margin: 30px 5px;">SAVE</button>

			 <button type="button" name="cancel"
 			 style="border-radius: 30px;
 			 background-color: #eee;
 			 width: 150px;
 			 margin: 30px 5px;">CANCEL</button>
			</td>
		</tr>

	</table>
</div>





</body>

</html>
