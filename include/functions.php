<?php

class header {

	public $title;


	public function __construct($title) {
		$this->title = $title;
	}

	public function start_header() {

$header = <<<_HTML
<!DOCTYPE html>
<head>
  <title>$this->title</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

_HTML;
echo $header;
	}

	public function navigation() {

$navi = <<<_HTML
<div id="top-nav" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Kredo IT Abroad</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-user"></i> Admin <span class="caret"></span></a>
                    <ul id="g-account-menu" class="dropdown-menu" role="menu">
                        <li><a href="#">My Profile</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="glyphicon glyphicon-lock"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>

_HTML;
echo $navi;

}


	public function side_navigation() {
		$side_navigation = <<<_HTML

		<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3" style="margin-top: 60px;">
 <ul class="nav nav-stacked">
                    <ul class="nav nav-stacked collapse in" id="userMenu">
                        <li class="active"> <a href="index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                        <li><a href="#"><i class="glyphicon glyphicon-envelope"></i> Messages <span class="badge badge-info">4</span></a></li>
                    </ul>
                </li>
                <hr>
                <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu2">IT Program Course <i class="glyphicon glyphicon-chevron-right"></i> </a>
                    <ul class="nav nav-stacked collapse" id="menu2">
                        <li><a href="#">Adobe Photoshop</a>
                        </li>
                        <li><a href="#">Adobe Illustrator</a>
                        </li>
                        <li><a href="#">HTML / CSS</a>
                        </li>
                        <li><a href="#">Wordpress</a>
                        </li>
                        <li><a href="#">PHP/MYSQL</a>
                        </li>
                    </ul>
                </li>
                <hr>
                 <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#menu3">ESL Program Course <i class="glyphicon glyphicon-chevron-right"></i> </a>
                    <ul class="nav nav-stacked collapse" id="menu3">
                        <li><a href="#">IT English Course</a>
                        </li>
                        <li><a href="#">Business English Course</a>
                        </li>
                        <li><a href="#">Basic ESL Course</a>
                        </li>
                    </ul>
                </li>

            </ul>

            <hr>

            <a href="#"><strong><i class="glyphicon glyphicon-link"></i> Resources</strong></a>

            <hr>

            <ul class="nav nav-pills nav-stacked">
                <li class="nav-header"></li>
                <li><a href="schedule.php?week=1&day=1"><i class="glyphicon glyphicon-list"></i> Schedule</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-briefcase"></i> Scorecard / Progress</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-pencil"></i> Assignments / Homeworks</a></li>
                <li><a href="assign.php?week=1&day=1"><i class="glyphicon glyphicon-calendar"></i> Assign Schedule</a></li>
                <li><a href="feedback.php"><i class="glyphicon glyphicon-retweet"></i> Student / Teacher Feedback</a></li>
                <li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#manage_student"><i class="glyphicon glyphicon-file"></i> Manage Students <i class="glyphicon glyphicon-chevron-right"></i> </a>
                    <ul class="nav nav-stacked collapse" id="manage_student">
                        <li><a href="add_student.php"><i class="glyphicon glyphicon-plus" style="margin-left: 30px;"></i> Add Students</a>
                        </li>
                        <li><a href="#"><i class="glyphicon glyphicon-minus" style="margin-left: 30px;"></i> Delete Students</a>
                        </li>
                       <li><a href="view_student.php"><i class="glyphicon glyphicon-th-list" style="margin-left: 30px;"></i> View Students</a>
                        </li>
                    </ul>
                </li>
                <li><a href="forms.php"><i class="glyphicon glyphicon-download"></i> Forms and Other Downloads</a></li>

            </ul>

            <hr>
            <ul class="nav nav-stacked">
                <li><a href="#"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
            </ul>

            <hr>
        </div>

_HTML;

echo $side_navigation;

	}



}

class menu {

	public $active;

	public function __construct($active) {

		$this->active = $active;

	}

	public function active_menu() {

		$button1 = $button2 = $button3 = $button4 = $button5 = '';

		switch($this->active) {



			case 'home':
			$button1 = "btn-danger";
			$button2 = $button3 = $button4 = $button5 = $button6 = "btn-success";
			break;

			case 'assessment':
			$button2 = "btn-danger";
			$button1 = $button3 = $button4 = $button5 = $button6 = "btn-success";
			break;

			case 'scorecard':
			$button3 = "btn-danger";
			$button1 = $button2 = $button4 = $button5 = $button6 = "btn-success";
			break;

			case 'schedule':
			$button4 = "btn-danger";
			$button1 = $button2 = $button3 = $button5 = $button6 = "btn-success";
			break;

			case 'program':
			$button5 = "btn-danger";
			$button1 = $button2 = $button3 = $button4 = $button6 = "btn-success";
			break;

			case 'assign':
			$button6 = "btn-danger";
			$button1 = $button2 = $button3 = $button4 = $button5 = "btn-success";
			break;

		}

$menu = <<<_HTML
<button class="btn $button1 btn-block"><span class="glyphicon glyphicon-home"></span><a href="#"> Home</a></button>
<button class="btn $button2 btn-block"><span class="glyphicon glyphicon-ok"></span> Assessment</button>
<button class="btn $button3 btn-block"><span class="glyphicon glyphicon-list-alt"></span><a href="scorecard.php"> Scorecard</a></button>
<button class="btn $button6 btn-block"><span class="glyphicon glyphicon-list-alt"></span><a href="assign.php"> Assign Schedule</a></button>
<div>
<button class="btn $button4 btn-block dropdown-toggle sidemenu" data-toggle="dropdown"><span class="glyphicon glyphicon-calendar"></span> Schedule <span class="caret"></span></button>
   <ul class="dropdown-menu" role="menu">
      <li><a href="schedule.php?type=daily">Daily</a></li>
      <li><a href="schedule.php?type=weekly">Weekly</a></li>
      <li><a href="schedule.php?type=monthly">Monthly</a></li>
    </ul>
    </div>
<div>

<button class="btn $button5 btn-block dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pencil"></span> Course Program <span class="caret"></span></button>
   <ul class="dropdown-menu" role="menu">
      <li><a href="program.php?q=ai">Adobe Illustrator</a></li>
      <li><a href="program.php?q=ps">Adobe Photoshop</a></li>
      <li><a href="#">HTML / CSS</a></li>
      <li><a href="#">Wordpress</a></li>
      <li><a href="#">Bootstrap</a></li>
      <li><a href="#">Javascript</a></li>
      <li><a href="#">PHP/MYSQL</a></li>
      <li><a href="#">Wordpress Portfolio</a></li>
      <li><a href="#">HTML/CSS Portfolio</a></li>
      <li><a href="#">PHP/MYSQL Portfolio</a></li>
    </ul>
 </div>

<button class="btn $button3 btn-block" style="margin-top:50px !important"><span class="glyphicon glyphicon-list-alt"></span><a href="scorecard.php"> Logout</a></button>

_HTML;

echo $menu;

	}
}


class connect_db {

	public $servername, $username, $password, $dbname;
	public $table, $student_name, $course, $duration, $category, $date_started, $date_completed;
	public $course_name, $course_duration;

	public function __construct($id, $table, $email, $pword) {
		$this->id = $id;
		$this->table = $table;
		$this->email = $email;
		$this->pword = $pword;
	}

	public function get_schedule() {
		require('../../.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT student_id, week, schedule_time, course_program, teacher, room FROM $this->table WHERE id='$this->id'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		$student_id = $row['student_id'];
		$week = $row['week'];
		$schedule_time = $row['schedule_time'];
		$course_program = $row['course_program'];
		$teacher = $row['teacher'];
		$room = $row['room'];
		$this->student_id = $student_id;
		$this->week = $week;
		$this->schedule_time = $schedule_time;
		$this->course_program = $course_program;
		$this->teacher = $teacher;
		$this->room = $room;

		$conn->close();
	}


	public function student_profile() {

		require('.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT id, batch, student_name, course, duration, category, date_started, date_completed FROM $this->table WHERE id='$this->id'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		$batch = $row['batch'];
		$student_name = $row['student_name'];
		$course = $row['course'];
		$duration = $row['duration'];
		$category = $row['category'];
		$date_started = $row['date_started'];
		$date_completed = $row['date_completed'];
		$this->batch = $batch;
		$this->student_name = $student_name;
		$this->course = $course;
		$this->duration = $duration;
		$this->category = $category;
		$this->date_started = $date_started;
		$this->date_completed = $date_completed;

		$conn->close();

	}

	public function student_course() {

		require('.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT id, course_name, course_duration, grade, remarks FROM $this->table WHERE id='$this->id'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		$course_name = $row['course_name'];
		$course_duration = $row['course_duration'];
		$grade = $row['grade'];
		$remarks = $row['remarks'];
		$this->course_name = $course_name;
		$this->course_duration = $course_duration;
		$this->grade = $grade;
		$this->remarks = $remarks;

		$conn->close();

	}

public function program_course() {

		require('.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT id, course_title, course_topic FROM $this->table WHERE id='$this->id'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		$course_title = $row['course_title'];
		$course_topic = $row['course_topic'];
		$this->course_title = $course_title;
		$this->course_topic = $course_topic;

		$conn->close();

	}

public function student_progress() {
		require('.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT id, student_id, course_status, course_feedback FROM $this->table WHERE id='$this->id'";
		$result = $conn->query($sql);
		$row = $result->fetch_assoc();
		$course_status = $row['course_status'];
		$course_feedback = $row['course_feedback'];
		$this->course_status = $course_status;
		$this->course_feedback = $course_feedback;

		$conn->close();

}

public function checkPass(){
		require('.connect');
		$conn = new mysqli($servername, $username, $password, $dbname);
		$sql = "SELECT email, password FROM $this->table WHERE email='$this->email' AND password='$this->pword'";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
		header("location: theme/default/index.php");
			}else {
		echo "<script>window.alert('Incorrect Username and/or Password. Please try again!')</script>";
	}
}

public function student_schedule_daily() {
	$daily = <<<_HTML
		<tr>
		<th class="text-center">Time</th>
		<th class="text-center">Course Topic</th>
		<th class="text-center">Teacher</th>
		<th class="text-center">Room</th>
		</tr>
_HTML;
		echo $daily;
	}

public function student_schedule_weekly() {
	$weekly = <<<_HTML
	<tr>
	<th class="text-center">Week</th>
	<th class="text-center">Course Program</th>
	<th class="text-center">Teacher</th>
	</tr>
_HTML;
	echo $weekly;
	}

public function student_schedule_monthly() {
	$monthly = <<<_HTML
	<tr>
	<th class="text-center">Month</th>
	<th class="text-center">Program Category</th>
	<th class="text-center">Duration (hrs)</th>
	</tr>
_HTML;
	echo $monthly;
	}


public function student_progress_update($course_id, $status, $feedback) {
	require('.connect');
	$this->course_id = $course_id;
	$this->status = $status;
	$this->feedback = $feedback;
	$conn = new mysqli($servername, $username, $password, $dbname);
	$all_status = implode (',', $this->status);
	$all_feedback = implode ('<n>', $this->feedback);
	$sql = "UPDATE $this->table SET course_status='$all_status', course_feedback='$all_feedback' WHERE id='$this->course_id'";
	if ($conn->query($sql) === TRUE) {
    echo "<script>location.reload();</script>"; 
    	} 
	}

public function save_student() {
	
}

}

function footer() {
$footer = <<<_HTML
<footer>
<div class="well" style="margin-top: 50px;"><p class="text-center">Student Information System by: Odysseus Ambut</p>
<p class="text-center">Version 0.9beta</p>
</div>
</footer>
_HTML;

echo $footer;
}

?>
