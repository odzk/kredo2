<?php require('include/functions.php'); 

if(empty($_POST['submit'])){
  $email = $pword = $_POST['submit'] = '';
} else {

  $email = $_POST['email'];
  $pword = $_POST['pword'];

  $check_pass = new connect_db('', 'user_access', $email, $pword);
  $check_pass->checkPass();
}
?>


<!DOCTYPE html>
<html>
<?php $header = new header('Student Information System Login');
$header->start_header();
?>

<body>
<div class="container" style="margin-top: 50px;">
<img src="assets/img/kredo-logo.jpg" class="img-responsive center-block" style="margin-bottom: 30px;">
	<section id="content">
		<form method="POST" action="">
			<h1>Login Form</h1>
			<div>
				<input type="text" placeholder="Email" required id="email" name="email" />
			</div>
			<div>
				<input type="password" placeholder="Password" required id="pword" name="pword" />
			</div>
			<div>
				<input type="submit" value="Log in" name="submit" style="margin-bottom: 0 !important;">
				<a href="#">Lost your password?</a>
				<a href="#">Register</a>
				<a href="#" style="margin-top: 0 !important;" onclick="wip()"><img src="assets/img/fb-login.png" width="50%""></a>
			</div>
		</form><!-- form -->
	</section><!-- content -->
</div><!-- container -->
</body>
<script>
function wip() {
	alert('Coming Soon!');
}  
</script>
</html>
